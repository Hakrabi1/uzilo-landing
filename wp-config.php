<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'uzilo' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'PKRyMrPL`HEL3upM+vK.a6?xu/?,YAiLajETq%M@i+o<g~S%(rb5OVh`.3Z_.jYZ' );
define( 'SECURE_AUTH_KEY',  'ne&8}Bh(dWP*MIfU/]FBv6*0_Z<~_f=$_Gl7)vrPmzZiAI(1M=d<HN_Y9H> i:IY' );
define( 'LOGGED_IN_KEY',    ';RO?:-5&.J=9Y+u8D2jL|2Fuf-BF<S-<a&pwqyxA0qp0|ceseKOp-XE&PXV#JFGc' );
define( 'NONCE_KEY',        '.P=J@c/rx8I:)/)s:@uvH-[X>.b,/UKcBt1d{^n>&8^!,b.3RY16<#U35?Tbo]~2' );
define( 'AUTH_SALT',        'L0&Po15cb)sCYZl^!5XuQ=4]k96UNv`k-<PSEn<E6(pWBX0?L ;oF8[674uRN&{L' );
define( 'SECURE_AUTH_SALT', '&K<p>C&igcr3{l9K#Yhc>N`<m8SfEV(xKDMOn7K]=$xxy6JkDY|$MDNU.*QS0C*B' );
define( 'LOGGED_IN_SALT',   ']L8SHvP%xzsOL&Fvvi^goUY-.,@IO<:J9pK8z/(Yu{F3x/Myp^/;Xw|zbA{j*B,#' );
define( 'NONCE_SALT',       'P,!~^cTVc6R]-gnzQE_!9&{m_CzmC{/LmyWm_sr(No1q4imwBL}sjIspHrmnOVzS' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
