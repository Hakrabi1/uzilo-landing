<?php
/*
Template Name: Home Page
*/
?>
<?php get_header(); 
$curr_dir_uri = get_template_directory_uri()
?>

<div id="wrapper">
	<div class="cover "></div>
	<div class="__modal">
		<div class="window d-flex align-items-center justify-content-center">
			<div class="content align-items-center justify-content-center">
				<div class="white-cover d-flex align-items-center justify-content-center flex-column ">
					<p class="title">coming soon</p>
					<p class="text">We’re currently working on the new kid on the block for remote user testing. Subscribe to stay up-to-date.</p>
					<div class="d-flex flex-row" >
						<input type="email" placeholder="Your email address ...">
						<div  class="input-button">
							<input type="submit" value="">
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</div>



<main>

	<section class="welcome">
		<div class="container">
			<div class="row justify-content-center">

				<div class="col-12 col-sm-auto">
					<h1 class="title">Test your	<span class="txt-rotate"
					data-period="2000"
     				data-rotate='[ "UX", "Website", "Design", "Idea", "App" ]'>
     				</span><br /> with real users</h1>
					<p class="welcome__entry">Fast. Simple. Affordable</p>
					<div class="welcome__list">
						<div class="welcome__list__element">
							<div class="listdash"></div><p>Hear what real users have to say about your product</p>
						</div>
						<div class="welcome__list__element">
							<div class="listdash"></div><p>Identify user painpoints and conversion killers</p>
						</div>
						<div class="welcome__list__element">
							<div class="listdash"></div><p>Get actionable insights to Improve your product</p>
						</div>		
					</div>

					<div class="d-flex align-items-center flex-column">
						<a href="" class="button open-modal" onclick="return false;">Sign up now</a>
						<div class="welcome__gift d-flex align-items-center flex-row">
							<img src="<?php echo $curr_dir_uri . '/img/gift.png' ?>" alt="">
							<p>Your first test is on us!</p>
						</div>
					</div>

				</div>
			
				<div class="col-12 col-lg">
					<img class="welcome__image" src="<?php echo $curr_dir_uri . '/img/app-screen.png' ?>" alt="">
				</div>

			</div>
		</div>
	</section>

	<section class="sense" id="can">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>Why user testing makes sense</h2>					
				</div>
			</div>		
			<div class="row justify-content-between">
				<div class="col-12 col-md-6">
					<div class="sense__block mr-lg-5 my-lg-5">
						<img src="<?php echo $curr_dir_uri . '/img/sense_1.svg' ?>" alt="">
						<div>
							<h4>Uncover product & design flaws</h4>
							<img src="<?php echo $curr_dir_uri . '/img/lines.svg' ?>" alt="" class="sense__block__lines">
						</div>
						<p> Real users will point you to bugs, conversion killers and roadblocks in your customer journeys that you might not have anticipated</p>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="sense__block ml-lg-5 my-lg-5">
						<img src="<?php echo $curr_dir_uri . '/img/sense_2.svg' ?>" alt="">	
						<div>
							<h4>Don’t gamble on product/ market fit</h4>
							<img src="<?php echo $curr_dir_uri . '/img/lines.svg' ?>" alt="" class="sense__block__lines">
						</div>
						<p>Getting real user feedback early & continuously in your product design process will save you money and time down the road - guaranteed!</p>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="sense__block mr-lg-5 my-lg-5">
						<img src="<?php echo $curr_dir_uri . '/img/sense_3.svg' ?>" alt="">
						<div>
							<h4>Positive impact on your bottom line</h4>
							<img src="<?php echo $curr_dir_uri . '/img/lines.svg' ?>" alt="" class="sense__block__lines">
						</div>
						<p>Addressing flaws uncovered during UX testing will have a positive effect on key drivers of your bottom line, e.g. conversion rates, bounce rates, or customer satisfaction scores</p>
					</div>
				</div>

				<div class="col-12 col-md-6">
					<div class="sense__block ml-lg-5 my-lg-5">
						<img src="<?php echo $curr_dir_uri . '/img/sense_4.svg' ?>" alt="">
						<div>
							<h4>It’s fast, simple and cheap!</h4>
							<img src="<?php echo $curr_dir_uri . '/img/lines.svg' ?>" alt="" class="sense__block__lines">
						</div>
						<p>UX Testing used to be a time & cost-intensive undertaking, typically done with the help of offline UX agencies. Not anymore - remote crowdtesting has changed the game for the better!</p>
					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="presentation">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>Launch proven ux tests within minutes</h2>

				</div>	

				<div class="col-12 col-lg-auto">
					<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/1.png' ?>" alt="">
							Usability test
						</a>
						<a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/2.png' ?>" alt="">
							Five second test
						</a>
						<a class="nav-link" id="v-pills-3-tab" data-toggle="pill" href="#v-pills-3" role="tab" aria-controls="v-pills-3" aria-selected="false">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/3.png' ?>" alt="">
							Preference test
						</a>
						<a class="nav-link" id="v-pills-4-tab" data-toggle="pill" href="#v-pills-4" role="tab" aria-controls="v-pills-4" aria-selected="false">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/4.png' ?>" alt="">
							First click Test
						</a>
						<a class="nav-link" id="v-pills-5-tab" data-toggle="pill" href="#v-pills-5" role="tab" aria-controls="v-pills-5" aria-selected="false">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/5.png' ?>" alt="">
							Navigation test
						</a>
						<a class="nav-link" id="v-pills-6-tab" data-toggle="pill" href="#v-pills-6" role="tab" aria-controls="v-pills-6" aria-selected="false">
							<img src="<?php echo $curr_dir_uri . '/img/presentation/6.png' ?>" alt="">
							Design questions
						</a>
						<p>... and more</p>
					</div>
				</div>
				
				<div class="col-12 col-lg">
					<div class="tab-content rectangle" id="v-pills-tabContent">
						<div class="tab-pane fade show active " id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
							<p>Watch real people interact with your website, app, or prototype and get their direct feedback on your product</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						<div class="tab-pane fade " id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
							<p>Measure a user’s first impression of your design or website to test it’s effectiveness in achieving your intended goals</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						<div class="tab-pane fade " id="v-pills-3" role="tabpanel" aria-labelledby="v-pills-3-tab">
							<p>Let a panel choose between different design variations and understand their reasoning</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						<div class="tab-pane fade " id="v-pills-4" role="tabpanel" aria-labelledby="v-pills-4-tab">
							<p>See where users would click first when presented with a task to find out how you can optimize your interface</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						<div class="tab-pane fade " id="v-pills-5" role="tabpanel" aria-labelledby="v-pills-5-tab">
							<p>Understand how users navigate through your website or app to uncover flaws in your site architecture</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						<div class="tab-pane fade " id="v-pills-6" role="tabpanel" aria-labelledby="v-pills-6-tab">
							<p>Get valuable user input on your designs by asking them a series of questions</p>
							<img src="<?php echo $curr_dir_uri . '/img/screen1.png' ?>" alt="">
						</div>
						
					</div>	
				</div>
				
				

			</div>
		</div>
	</section>
	

	<div class="clients">
		<div class="container">
			<div class="row justify-content-around justify-content-md-between align-items-center">
				<div class="col-12">
					<h6>
						Thousands of clients - from small business owners and digital nomads to product teams in large companies - trust our UX testing platform
					</h6>
				</div>


				<div class="col-4 col-md-2 ">
					<img src="<?php echo $curr_dir_uri . '/img/clients-logos/1.png'?>" alt="">
				</div>

				<div class="col-4 col-md-2">
					<img src="<?php echo $curr_dir_uri . '/img/clients-logos/2.png'?>" alt="">
				</div>

				<div class="col-4 col-md-2">
					<img src="<?php echo $curr_dir_uri . '/img/clients-logos/3.png'?>" alt="">
				</div>

				<div class="col-4 mt-4 mt-md-0 col-md-2">
					<img src="<?php echo $curr_dir_uri . '/img/clients-logos/4.png'?>" alt="">
				</div>

				<div class="col-4 mt-4 mt-md-0 col-md-2">
					<img src="<?php echo $curr_dir_uri . '/img/clients-logos/5.png'?>" alt="">
				</div>

			</div>
		</div>
	</div>

	
	<section class="algorithm">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>UX Testing made easy with uzilo</h2>
				</div>
			</div>

		    <div class="row algorithm__row">
		    	<div class="col-12 col-md-6 d-flex justify-content-center flex-column">
		    		<div class="mr-lg-5">
		    			<h3>1. Design your UX test</h3>
		    			<div class="line"></div>
		    			<p>Our simple-to-use, yet powerful test wizard helps you create UX tests in a breeze! And if you do happen to run into questions, our support team will be right there for you!</p>
		    		</div>
		    	</div>
				<div class="col-12 col-md-6 d-flex justify-content-center justify-content-md-start">
					<div class="rectangle d-flex align-items-center ml-lg-5">
						<img data-aos="fade-right" src="<?php echo $curr_dir_uri . '/img/screen.png'?>" alt="">
					</div>
				</div>
		    </div>

			<div class="row algorithm__row">
				<div class="col-12 col-md-6 order-md-4 d-flex justify-content-center flex-column">
					<div class="ml-lg-5">
						<h3>2. Select your tester panel</h3>
				    	<div class="line"></div>
				    	<p>Take advantage of our large, diverse, and dedicated tester panel of over 50.000 testers. We provide comprehensive targeting options to help you drive relevant results.</p>
					</div>
				</div>
				<div class="col-12 col-md-6 order-md-3 d-flex justify-content-end">
			    	<div class="rectangle d-flex align-items-center justify-content-end mr-lg-5">
						<img data-aos="fade-left" src="<?php echo $curr_dir_uri . '/img/screen.png'?>" alt="">
					</div>
			    </div>
		    </div>

			<div class="row algorithm__row">
			    <div class="col-12 col-md-6 d-flex justify-content-center flex-column">
			    	<div class="mr-lg-5">
			    		<h3>3. Evaluate the responses</h3>
			    		<div class="line"></div>
			    		<p>We have built powerful tools to assist you in analysing user feedback and generating actionable insights. Strong visualizations, audio transcriptions, collaboration features and much more!</p>
			    	</div>
			    </div>
				<div class="col-12 col-md-6 justify-content-center justify-content-md-start">
					<div class="rectangle d-flex align-items-center ml-lg-5">
						<img data-aos="fade-right" src="<?php echo $curr_dir_uri . '/img/screen.png'?>" alt="">
					</div>
				</div>
			</div>

		</div>
	</section>
	

	<section class="targeting">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2>comPrehensive targeting options</h2>
					<p>We know how important it is to target your tests to specific audiences. That’s why you can select a large number of demographic options or - in case you want to get even more specific - add screener questions to your test!</p>
				</div>
				
				<div class="col-12 d-flex justify-content-between flex-column flex-sm-row">
					<ul>
						<li>Age</li>
						<li>Gender</li>
						<li>Household Income</li>
						<li>Employment status</li>
					</ul>

					<ul>
						<li>Languages spoken</li>
						<li>Location</li>
						<li>Family situation</li>
						<li>Education</li>
					</ul>

					<ul>
						<li>Streaming behaviour</li>
						<li>Shopping behaviour</li>
						<li>Technical proficiency</li>
						<p>... and more!</p>
					</ul>
				</div>
			</div>
		</div>
	</section>
	

	<section class="pricing">
		<div class="container">
			<div class="row">

				<div class="col-12">
					<div class="pricing__block">
						<h3>Simple & transparent pricing</h3>
						<div class="container">
							<div class="row justify-content-around">
								<div class="col-12 col-md-6 col-lg-5">
									<h2>Usability Tests</h2>
									<div class="line"></div>
									<p>only</p>
									<p class="price">$ 49</p>
									<p>per response</p>
								</div>
							
								<div class="col-12 col-md-6 col-lg-5">
									<h2>User Research Tests</h2>
									<div class="line"></div>
									<p>starting at</p>
									<p class="price">$ 1</p>
									<p>per response</p>
								</div>
							</div>
						</div>
					</div>

					<div class="pricing__try-now">
						<h2>Sign up now.</h2> 
						<h2>Your first test is on us.</h2>
						<a class="button open-modal" href='' onclick="return false;">Try now for free!</a>
					</div>
				</div>

			</div>
		</div>
	</section>	

<section class="faq">
	<div class="container">
		<h2>frequently asked questions</h2>
		<a class="accordion d-flex align-items-center justify-content-between">What is a usability test? <div class="angle"></div></a>
		<div class="panel">
		  <p>Usability tests are used to validate design decisions of product interfaces (e.g. websites or apps) by testing them with real users. Such users run through a series of tasks while using the respective product, all while recording their screen and voice at the same time. Usability tests provide a valuable resource to detect product flaws or unforeseen roadblocks in customer journeys. The objective and unbiased view of users not involved in the product design process helps uncover blind spots and mitigate tunnel vision.</p>
		</div>

		<a class="accordion d-flex align-items-center justify-content-between"> What is the difference between a usability test and a user research test?<div class="angle"></div></a>
		<div class="panel">
		  <p>In the world of UX, definitions are many-fold and so differentiating one from another is not always straight forward. Technically speaking usability tests belong under the umbrella of user research tests, but we differentiate the two because while usability tests focusses on uncovering usability issues with a product (e.g. unclear navigation), user research tests provides deeper insights into user preferences, and behaviors (e.g. what is the lasting impression of seeing a website for 5 seconds). Also, while usability tests are typically done with a smaller panel size of up 10 users, user research tests are quick-response tests that can be completed by large panels in a shorter amount of time.</p>
		</div>

		<a class="accordion d-flex align-items-center justify-content-between">At what stage of a project design process should we involve users?<div class="angle"></div></a>
		<div class="panel">
		  <p>It’s never too early to involve real users into your product design process. Early feedback can help validate ideas, find flaws in your customer journey, and uncover bugs that your team might have overlooked. Remote user testing is affordable and offers quick turnaround times, so why don’t you try smaller-scale testing early on in the process to see whether or not it is value-creating to you?</p>
		</div>
		
		<a href="#" class="button">Go to FAQs</a>

	</div>
</section>


<section class="quote">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-6 ">
				<img src="<?php echo $curr_dir_uri . '/img/quote_img.png'?>" alt="" class="quote__img">
			</div>
			<div class="col-12 col-lg-6 d-flex justify-content-center flex-column">
				<p class="quote__text">“Usability testing is never a waste of time, even if you think you know the answer”</p>
				<p class="quote__author">Nielsen Norman Group</p>
			</div>
		</div>
	</div>
</section>

<section class="try-now">
	<div class="container">
		<h2>access to thousands of testers with just a few clicks</h2>
		<p class="text">Over 50.000 testers work with us to help our clients solve their UX challenges. Best-in-class testing software, diligent tester selection & strong quality control measures are hallmarks of why we truly believe that our tester panel can drive the best results.</p>
		<a href="" class="button open-modal" onclick="return false;">Try now for free!</a>

		<div class="try-now__gift d-flex justify-content-center flex-row">
			<img src="<?php echo $curr_dir_uri . '/img/gift.png' ?>" alt="">
			<p>Your first test is on us!</p>
		</div>

	</div>
</section>
	
</main>
<?php get_footer() ?>