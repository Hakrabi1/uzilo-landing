<!DOCTYPE html>
<html lang="en">
<?php wp_head(); 
$curr_dir_uri = get_template_directory_uri()?>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Uzilo</title>
</head>
<body>
	
<header class="header">
	<div class="container">
		<!-- <div class="row">
			<div class="col-3">
				<img src="<?php echo $curr_dir_uri . '/img/logo.svg' ?>" alt="" class="logo">
			</div>
			<div class="col-9">
				<div class="header__links d-flex align-items-center justify-content-between">
					<a href="#" class="header__links__link hidden">How it works</a>
					<a href="#" class="header__links__link hidden">Pricing</a>
					<a href="#" class="header__links__link hidden">Blog</a>
					<a href="#" class="header__links__link hidden">Use Cases</a>
					<a href="#" class="header__links__link">Become a tester</a>
					<div class="d-flex align-items-center justify-content-end">
						<a href="#" class="header__links__button sign-in">Sign in</a>
						<a href="#" class="header__links__button sign-up">Sign up</a>
					</div>
					
				</div>
			</div>
		</div> -->
				<nav class="navbar navbar-expand-lg navbar-light">
					<a class="navbar-brand" href="#">
						<img src="<?php echo $curr_dir_uri . '/img/logo.svg' ?>" alt="" class="logo">
					</a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
						<div class="navbar-nav header__links d-flex align-items-md-center justify-content-between flex-column flex-md-row">
							<a href="#" class="header__links__link hidden">How it works</a>
							<a href="#" class="header__links__link hidden">Pricing</a>
							<a href="#" class="header__links__link hidden">Blog</a>
							<a href="#" class="header__links__link hidden">Use Cases</a>
							<a href="#" class="header__links__link">Become a tester</a>
							<div class="d-flex align-items-center  justify-content-center justify-content-lg-end">
								<a href="#" class="header__links__button sign-in">Sign in</a>
								<a href="#" class="header__links__button sign-up">Sign up</a>
							</div>
						</div>
					</div>
				</nav>	
		<!-- </div> -->
	</div>
</header>