var gulp         = require('gulp');
//var browserSync  = require('browser-sync').create();
var sass         = require('gulp-sass');
var watch 		 = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var concatCss    = require('gulp-concat-css');
var cleanCSS     = require('gulp-clean-css');
var rename       = require("gulp-rename");
var uglify       = require('gulp-uglify');

gulp.task('scss', function(done) {
	gulp.src("src/scss/*.scss")
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		overrideBrowserslist: ['last 2 versions'],
		cascade: false
	}))
	.pipe(concatCss("style.css"))
	.pipe(gulp.dest("src/css"));
	done();
});

gulp.task('mincss', function(done) {
	gulp.src("src/css/*.css")
	.pipe(rename({suffix: ".min"}))
	.pipe(cleanCSS())
	.pipe(gulp.dest("app/css"));
	done();
})

gulp.task('minjs', function(done) {
	gulp.src("src/js/*.js")
	.pipe(rename({suffix: ".min"}))
	.pipe(uglify())
	.pipe(gulp.dest("app/js"));
	done();
})


gulp.task('watch', function(){
	gulp.series('mincss');
	gulp.watch("src/scss/*.*", gulp.series('scss'));
	
	/*watch(path.watch.js, function(event, cb) {
		gulp.start('js:build');
	});
	watch(path.watch.img, function(event, cb) {
		gulp.start('image:build');
	});
	watch(path.watch.fonts, function(event, cb) {
		gulp.start('fonts:build');
	});*/
});

gulp.task('min', gulp.series('mincss', 'minjs'));

gulp.task('default', gulp.series('scss', 'mincss', 'minjs'));