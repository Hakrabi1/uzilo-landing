
<footer>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-5 order-3 order-lg-1 d-flex align-items-center justify-content-center justify-content-lg-start flex-row ">
				<img src="<?php echo get_template_directory_uri() . '/img/union.svg' ?>" alt="" class="union">
				<p>2020 Uzilo.com All Rights Reserved</p>
			</div>
			<div class="col-9 col-lg-5 order-1 order-lg-2 d-flex align-items-center justify-content-around  flex-row">
				<a href="#" class="link">Privacy Policy</a>					
				<a href="#" class="link">Terms & Conditions</a>					
			</div>	
			<div class="col-3 col-lg-2 order-2 order-lg-3 d-flex align-items-center justify-content-around flex-row">
				<a href="#" class="icon">
					<img src="<?php echo get_template_directory_uri() . '/img/facebook.svg' ?>" alt="">
				</a>					
				<a href="#" class="icon">
					<img src="<?php echo get_template_directory_uri() . '/img/twitter.svg' ?>" alt="">
				</a>					
			</div>	
		</div>
	</div>
</footer>
	<?php wp_footer(); ?>
</body>
</html>