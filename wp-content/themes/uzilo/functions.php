<?php 
add_action( 'wp_enqueue_scripts', 'uzilo_scripts' );

function uzilo_scripts() {
	wp_enqueue_script( 'jqueryl', get_template_directory_uri() . '/app/js/jquery-3.5.1.min.js', array(), '3.5.1', true );
	wp_enqueue_script( 'bootstrapjs', get_template_directory_uri() . '/app/js/bootstrap.min.js', array(), '4.1.3', true);
	wp_enqueue_script( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), '1.0.0', true );
	wp_enqueue_script( 'main', get_template_directory_uri() . '/src/js/main.js', array(), '1.0.0', true );

	wp_enqueue_style( 'bootstrapcss', get_template_directory_uri() . '/app/css/bootstrap.css' );
	//wp_enqueue_style( 'signika', 'https://fonts.googleapis.com/css2?family=Signika:wght@300;400;600;700&display=swap' );
	//wp_enqueue_style( 'main', get_template_directory_uri() . '/app/css/style.min.css' );
	wp_enqueue_style( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/src/css/style.css' );
}

?>